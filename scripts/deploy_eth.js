const { ethers } = require("hardhat");

async function deployERC20() { 
    const eFundERC20 = await ethers.getContractFactory("eFundERC20");
    return await eFundERC20.deploy();
}

async function deployContractFactory() { 
    const Factory = await ethers.getContractFactory("FundFactory");
    return await Factory.deploy();
}

async function deployEFundPlatform(factory,erc20) { 
    const Platform = await ethers.getContractFactory("EFundPlatform");
    return await Platform.deploy(factory.address,erc20.address, 100000000000000000n, 1000000000000000000000n);
}


async function main() {
    var erc20 = await deployERC20();
    console.log("eFundERC20 deployed to: '\x1b[36m%s\x1b[0m'", erc20.address);

    var factory = await deployContractFactory(erc20);
    console.log("Factory deployed to: '\x1b[36m%s\x1b[0m'", factory.address);

    var platform = await deployEFundPlatform(factory,erc20);
    console.log("EFundPlatform deployed to: '\x1b[36m%s\x1b[0m'", platform.address);
}



  main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });